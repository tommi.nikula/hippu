import logging

from hippu import Server
from hippu import BaseService
from hippu.filters import GzipFilter


class MyService(BaseService):
    """ Custom service. """

    def match(self, request):
        """ Returns matching handler for the provided request or None.

        Returned handler must be a callable that accept 'request' and 'response'
        arguments. Returning None indicates that this service cannot serve the
        provided request. Server will try next mounted service or responds
        HTTP/1.0 404 Not Found.

        Note: create_resolver() creates a wrapper that implements the
        afforementioned mentioned API and calls the handler via a filter chain.
        It also provides the service context for the handler as third argument.

        For example,

            def match(self, request):
                def _handler(request, response):
                    pass

                return _handler
        """
        return self.create_resolver(self)

    def __call__(self, request, response, context):
        """ Handles all requests. """
        response.content = "This is a custom service running in '{}' mode!".format(context.mode)


# Set up logging
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

# Server address.
address = ('localhost', 8080)

# Set up a custom service with as little overhead as possible. It is
# possible to use filters and context variables with custom services.
service = MyService()
service.add_filter(GzipFilter)
service.update_context(mode='debug')

# Set up a server and mount the service (or services).
httpd = Server(address)
httpd.mount(service)

try:
    # Start server.
    httpd.serve_forever()
except KeyboardInterrupt:
    httpd.server_close()
