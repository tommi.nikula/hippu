import datetime
import io
import logging

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

from hippu import Server
from hippu import Service

log = logging.getLogger(__name__)


def create_image():
    date = datetime.datetime.now()
    image = Image.new('RGB', (600, 400), color='white')
    draw = ImageDraw.Draw(image)
    draw.text((10, 10), str(date), fill=(20, 20, 20))
    return image


def upload_image(req, res, ctx):
    global image

    image = Image.open(req.form['photo'].file)
    image.load()


def fetch_image(req, res, ctx):
    res.content = image


# Set up logging
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

image = create_image()

# Server address
address = ('localhost', 8080)

# Create routes
service = Service()
service.map('GET', '/image', fetch_image)
service.map('POST', '/image', upload_image)

# Set up server
httpd = Server(address)

# Mount routes
httpd.mount(service)

try:
    # Start the server
    httpd.serve_forever()
except KeyboardInterrupt:
    httpd.server_close()

