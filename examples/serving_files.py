import logging

from hippu import Server
from hippu import FileService
from hippu.filters import GzipFilter


class Cache:
    """ Simple cache implementation to demonstrate how to write a custom filters.

    Cache filter monitors HTTP GET requests. It caches response content and headers.
    Cached content is returned when the same resource is requested again.

    Implementation is naive but demonstrates the idea nicely.
    """
    def __init__(self):
        self._cache = {}

    def __call__(self, req, res, ctx, next):
        """ Filters can carry out pre- and post processing tasks for requests.

        Filter gets 'request', 'response', 'context' objects and a callable
        ('next') that should be called by the filter. Filters can modify
        request, response and context objects.

        Filter can decide whether it's calling the given callable or resolves
        the request by itself (e.g., when user is not authorized to access
        the target resource).
        """
        # Pre-processing.
        if req.method == 'GET':
            if req.path in self._cache:
                res.headers, res.content = self._cache[req.path]
                return

        # Call the next callable in the line. Filter can brake the chain
        # and resolve request - like above.
        next(req, res, ctx)

        # Post-processing: store response headers and content.
        if req.method == 'GET':
            self._cache[req.path] = (res.headers, res.content)


# Set up logging.
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

# Server address.
address = ('localhost', 8080)

# Create a servive to serve static files from given path. Default path is
# './static'.
service = FileService(path='./static')

# Add request filters. Filters will be applied in the same order as added.
service.add_filter(Cache)
service.add_filter(GzipFilter)

# Set up the server and mount the service (or services).
httpd = Server(address)
httpd.mount(service)

try:
    # Start the server.
    httpd.serve_forever()
except KeyboardInterrupt:
    httpd.server_close()
