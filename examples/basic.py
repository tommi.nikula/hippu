from hippu import Server
from hippu import Service


# Request handler.
def say_hello(req, res, ctx):
    res.content = 'Hello!'


# Set up a service.
service = Service()

# Map a route to serve HTTP GET 'http://localhost:8080/hello' requests.
service.map('GET', '/hello', say_hello)

# Server address.
address = ('localhost', 8080)

# Set up server.
httpd = Server(address)

# Mount the service.
httpd.mount(service)

try:
    # Start server.
    httpd.serve_forever()
except KeyboardInterrupt:
    httpd.server_close()
