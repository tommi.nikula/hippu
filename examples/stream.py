import io
import datetime
import logging
import time
import threading
from queue import Queue

try:
    from PIL import Image
    from PIL import ImageDraw
    from PIL import ImageFont
except ImportError:
    print("This example requires image libary (https://pypi.org/project/Pillow).",
          "You can install Pillow with pip:",
          "$ pip install Pillow",
          sep='\n')
    exit()

try:
    import cv2
    import numpy
except ImportError:
    print("OpenCV and/or Numpy not installed. Some optional examples might not work.")

from hippu import Server
from hippu import Service
from hippu.http import Header
from hippu import MJPEGStream
from hippu import ChunkedStream

log = logging.getLogger(__name__)


class PILImageFactory:
    @classmethod
    def create_image(cls, size=(1024, 768)):
        font = ImageFont.load_default()
        title = datetime.datetime.now().isoformat()
        image = Image.new('RGB', size, color='green')
        draw = ImageDraw.Draw(image)
        draw.text((10, 10), title, font=font, fill=(255, 255, 0))
        return image

    @classmethod
    def get_bytes(cls, image):
        with io.BytesIO() as output:
            image.save(output, format="JPEG")
            return output.getvalue()


class NumpyImageFactory(PILImageFactory):
    @classmethod
    def create_image(cls, size=(1024, 768)):
        img = super().create_image(size)
        return numpy.asarray(img, dtype="uint8")

    @classmethod
    def get_bytes(cls, image):
        return cv2.imencode(ext='.jpg', img=image)[1]


def mjpeg_stream(req, res, ctx):
    """ Motion JPEG (mjpeg) stream. """
    factory = PILImageFactory
    # factory = NumpyImageFactory

    with MJPEGStream(res) as stream:
        # Stream's __bool__ method returns true while stream is
        # open for write (stream.is_open()).
        while stream:
            image = factory.create_image()

            # Put image in to stream.
            stream.put(image)

            # Or write the bytes.
            # stream.write(factory.get_bytes(image))

    # Previous example enhanced with buffering.
    #
    # def producer(stream, queue):
    #     while stream:
    #         queue.put(factory.create_image())

    # with MJPEGStream(res) as stream:
    #     # Queue is used to hold only the latest image (producer can produce
    #     # more images than we can deliver).
    #     queue = Queue(maxsize=1)
    #
    #     # Producer thread.
    #     threading.Thread(target=producer, args=(stream, queue)).start()
    #
    #     # Consuming images.
    #     while stream:
    #         stream.put(queue.get())

    log.info("Stream closed.")


def data_stream(req, res, ctx):
    """ Chunked data stream.

    Example usage:
        $ curl -N http://localhost:8080/stream
    """
    with ChunkedStream(res, 'text/plain') as stream:
        # Loops as long as stream is open. Stream's __bool__ method returns true
        # if stream is open. Instead, it's also possible to use stream.is_open().
        while stream:
            # Data must be bytes.
            stream.write("hello world\n".encode('utf-8'))
            time.sleep(1)

        log.info("Stream closed.")


# Set up logging.
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.WARNING)

service = Service()
service.map('GET', '/mjpeg', mjpeg_stream)
service.map('GET', '/stream', data_stream)

httpd = Server(address=('localhost', 8080))
httpd.mount(service)

try:
    httpd.serve_forever()
except KeyboardInterrupt:
    httpd.server_close()
