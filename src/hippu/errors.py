class InvalidContentType(Exception):
	""" Request content type does not match expectation. """
	pass