# This file is purely for defining package metadata. No logic or importing the
# module code is allowed here.
# This file will be evaluated by setup.py as a standalone namespace, so neither
# __file__ nor __name__ are defined.
__all__ = ['__author__', '__license__', '__version__', '__credits__',
           '__maintainer__']

__author__ = 'Sami Laine'
__license__ = 'BSD-3-Clause'
__version__ = '0.8.0'
__credits__ = ['Sami Laine']
__maintainer__ = 'Sami Laine'
