import pytest
import time
import threading
import requests

from hippu import Server
from hippu import Service
from hippu.http import Header
from hippu.http import Status


address = ('localhost', 8080)


def test_instantiation():
    s = Server(address)
    s.server_close()


def test_create():
    s = Server.create(address)
    s.server_close()


def test_mount_service():
    service = Service('Test')
    server = Server(address)
    server.mount(service)
    server.unmount(service)
    server.server_close()


def test_serve_request():
    def test_filter(req, res, ctx, next):
        next(req, res, ctx)

    def do_get(req, res, ctx):
        res.status_code = 200
        res.content = dict(a='b')

    def do_put(req, res, ctx):
        res.status_code = 204

    def do_post(req, res, ctx):
        res.status_code = 201

    def do_delete(req, res, ctx):
        res.status_code = 204

    def start_server(server):
        try:
            server.serve_forever()
        except KeyboardInterrupt:
            server.shutdown()

    service = Service('Test')
    service.map('GET', '/', do_get)
    service.map('PUT', '/', do_put)
    service.map('POST', '/', do_post)
    service.map('DELETE', '/', do_delete)

    service.add_filter(test_filter)

    server = Server(address)
    server.mount(service)

    threading.Thread(target=start_server, args=(server, )).start()

    url = "http://{}:{}".format(address[0], address[1])

    try:
        response = requests.get(url)
        assert response.status_code == 200
        assert response.json()['a'] == 'b'
        assert response.headers['content-type'] == str(Header.APPLICATION_JSON).lower()

        response = requests.post(url)
        assert response.status_code == 201

        response = requests.put(url)
        assert response.status_code == 204

        response = requests.delete(url)
        assert response.status_code == 204
    finally:
        server.shutdown()
        server.server_close()


# def test_add_filter():
#     def test_filter(req, res, ctx, next):
#         next(req, res, ctx)

#     server = Server(address)
#     server.add_filter(test_filter)
