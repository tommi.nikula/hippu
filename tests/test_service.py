from hippu import Service
from hippu.request import Path


class Request:
    def __init__(self, method, path):
        self.path = Path(path)
        self.method = method


def test_instantiate():
    Service()
    Service('test')


def test_map():

    def collection(req, res, ctx): pass

    def resource(req, res, ctx): pass

    service = Service()

    service.map('GET', '/collection', collection)
    service.map('GET', '/collection/{id}', resource)
    service.map('GET', '/collection-with-str-id/{id:str}', resource)
    service.map('GET', '/collection-with-int-id/{id:int}', resource)
    service.map('GET', '/collection-with-float-id/{id:float}', resource)
    service.map('GET', '/collection-with-mixed-id/{id1:int}/{id2:float}/{id3:str}', resource)

    handler = service.match(Request('GET', '/collection'))
    assert handler is not None
    assert handler._handler == collection

    handler = service.match(Request('GET', '/collection/a'))
    assert handler is not None
    assert handler._handler == resource
    assert handler._context['id'] == 'a'

    handler = service.match(Request('GET', '/collection-with-str-id/1'))
    assert handler is not None
    assert handler._handler == resource
    assert handler._context['id'] == '1'

    handler = service.match(Request('GET', '/collection-with-int-id/1'))
    assert handler is not None
    assert handler._handler == resource
    assert handler._context['id'] == 1

    handler = service.match(Request('GET', '/collection-with-float-id/1.0'))
    assert handler is not None
    assert handler._handler == resource
    assert handler._context['id'] == 1.0

    handler = service.match(Request('GET', '/collection-with-mixed-id/1/2.0/test'))
    assert handler is not None
    assert handler._handler == resource
    assert handler._context['id1'] == 1
    assert handler._context['id2'] == 2.0
    assert handler._context['id3'] == 'test'


def test_route_context():
    def handler(req, res, ctx):
        pass

    service = Service()
    service.map('GET', '/', handler, context=dict(a=1))

    r = service.match(Request('GET', '/'))
    assert r is not None
    assert r._context['a'] == 1
