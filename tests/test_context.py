from hippu.service import Context

def test_create_context():
    ctx = Context.create()
    assert ctx is not None


def test_put_into_context():
    ctx = Context()
    ctx.put('a', 1)

    assert ctx.a == 1


def test_update_context():
    ctx = Context()
    ctx.update(dict(b=2, c=3))

    assert ctx.b == 2
    assert ctx.c == 3


def test_copy_context():
    ctx1 = Context(dict(a=2, b=2))
    ctx2 = ctx1.copy()

    ctx1.put('c', 1.3)
    ctx2.put('c', 2.3)

    assert ctx1 != ctx2
    assert ctx1.a == ctx2.a
    assert ctx1.b == ctx2.b
    assert ctx1.c == 1.3
    assert ctx2.c == 2.3
