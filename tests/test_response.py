from hippu.response import Response
from hippu.http import Header
from hippu.http import Status


def test_set_and_get_header():
    r = Response(None)
    r.set_header('a', 'b')

    assert r.get_header('a') == 'b'


def test_cookie():
    r = Response(None)
    r.cookie['a'] = 'b'

    assert r.cookie['a'].value == 'b'


def test_finalize_with_bytes():
    r = Response(None)
    r.content = b'1'
    r.finalize()

    assert r.content == b'1'


def test_finalize_with_str():
    r = Response(None)
    r.content = 'a'
    r.finalize()

    assert r.content == b'a'
    assert r.content_type == Header.TEXT_HTML


def test_finalize_with_int():
    r = Response(None)
    r.content = 42
    r.finalize()

    assert r.content == b'42'
    assert r.content_type == Header.TEXT_HTML


def test_finalize_with_float():
    r = Response(None)
    r.content = 42.0
    r.finalize()

    assert r.content == b'42.0'
    assert r.content_type == Header.TEXT_HTML


def test_finalize_with_list():
    r = Response(None)
    r.content = [42]
    r.finalize()

    assert r.content == b'[42]'
    assert r.content_type == Header.APPLICATION_JSON


def test_finalize_with_dict():
    r = Response(None)
    r.content = dict(a=42)
    r.finalize()

    assert r.content == b'{"a": 42}'
    assert r.content_type == Header.APPLICATION_JSON


def test_finalize_with_img():
    try:
        from PIL import Image
    except ImportError:
        return

    r = Response(None)
    r.content = Image.new('RGB', (600, 400))
    r.finalize()

    assert r.content_type == Header.IMAGE_PNG

    r.content_type = Header.IMAGE_BMP
    r.finalize()

    assert r.content_type == Header.IMAGE_BMP

    r.content_type = Header.IMAGE_GIF
    r.finalize()

    assert r.content_type == Header.IMAGE_GIF

    r.content_type = Header.IMAGE_JPEG
    r.finalize()

    assert r.content_type == Header.IMAGE_JPEG

    r.content_type = Header.IMAGE_PNG
    r.finalize()

    assert r.content_type == Header.IMAGE_PNG

# def test_finalize_with_class():
#     class Test:
#         def __str__(self):
#             return "OK"

#     r = Response(None)
#     r.content = Test()
#     r.finalize()

#     assert r.content == b'OK'
#     assert r.content_type == HTTP.TEXT_HTML
